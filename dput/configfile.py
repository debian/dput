# dput/configfile.py
# Part of ‘dput’, a Debian package upload toolkit.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

""" Program configuration file functionality. """

import collections
import configparser
import os.path
import sys
import textwrap

import xdg


DEFAULT_HOST_MAIN = "ftp-master"
""" Destination host name default value. """


class ConfigurationError(RuntimeError):
    """ Exception raised when failing to configure the program. """


default_config_paths = collections.OrderedDict([
        ('global', os.path.join(os.path.sep, "etc", "dput.cf")),
        ('user_xdg', os.path.join(
            xdg.xdg_config_home(), "dput", "dput.cf")),
        ('user_home', os.path.join(
            os.path.expanduser("~"), ".dput.cf")),
])


def active_config_files(config_path, debug):
    """ Yield the sequence of configuration files active for this process.

        :param config_path: Filesystem path of config file to read.
        :param debug: If true, enable debugging output.
        :return: Generator for the sequence of configuration files.
        :raise ConfigurationError: When none of the candidate configuration
            files can be opened.

        The `config_path` specifies a single candidate configuration file path
        to open. If `None`, instead use the default candidate files:

        * The global configuration file ‘/etc/dput.cf’.
        * The user configuration file:
          * $XDG_CONFIG_HOME/dput/dput.cf or, if that does not exist:
          * $HOME/.dput.cf

        Each file is open for reading when yielded in the sequence. When a
        candidate file fails to open (because it does not exist, the process
        does not have read permission to the file, etc.), that file is skipped.
        If all candidate files fail to open, raise `ConfigurationError`.
        """
    def candidate_config_paths(config_path, debug):
        if config_path:
            # Configuration file path explicitly specified to this process.
            yield config_path
        else:
            # Default configuration file paths.
            yield from default_config_paths.values()

    attempted_file_paths = []
    active_file_paths = []
    for infile_path in candidate_config_paths(config_path, debug):
        if not config_path:
            # Iterating through the default configuration paths.
            if (default_config_paths['user_xdg'] in active_file_paths):
                # We already have the standard user configuration file.
                if (infile_path == default_config_paths['user_home']):
                    # No need to try the deprecated location.
                    continue

        attempted_file_paths.append(infile_path)
        try:
            infile = open(infile_path, encoding='UTF-8')
        except OSError as exc:
            if debug:
                sys.stderr.write(
                        "{error}: skipping ‘{path}’\n".format(
                            error=str(exc), path=infile_path))
            continue
        active_file_paths.append(infile_path)
        yield infile

    if not active_file_paths:
        paths_text = ", ".join(
                "‘{0}’".format(path)
                for path in attempted_file_paths)
        raise ConfigurationError(
                "Could not open any configfile, tried {paths}".format(
                    paths=paths_text))


def read_configs(config_files=None, *, debug=False):
    """ Read configuration settings from config files.

        :param config_files: Sequence of configuration files, each open for
            reading.
        :param debug: If true, enable debugging output.
        :return: The resulting `ConfigParser` instance.

        The config parser will parse each file in `config_files`. Configuration
        from later files overrides earlier files.
        """
    config = configparser.ConfigParser()

    config.read_dict({
            config.default_section: {
                'login': "username",
                'method': "scp",
                'hash': "md5",
                'allow_unsigned_uploads': False,
                'allow_dcut': False,
                'distributions': "",
                'allowed_distributions': "",
                'run_lintian': False,
                'run_dinstall': False,
                'check_version': False,
                'scp_compress': False,
                'default_host_main': "",
                'post_upload_command': "",
                'pre_upload_command': "",
                'ssh_config_options': "",
                'passive_ftp': True,
                'progress_indicator': "0",
                'delayed': "",
                },
            })

    for config_file in config_files:
        if debug:
            sys.stdout.write(
                    "D: Parsing configuration file ‘{path}’\n".format(
                        path=config_file.name))
        try:
            config.read_file(config_file)
        except configparser.ParsingError as e:
            sys.stderr.write(textwrap.dedent("""\
                    Error parsing configuration file:
                    {0}
                    """).format(str(e)))
            sys.exit(1)
        config_file.close()

    # only check for fqdn and incoming dir, rest have reasonable defaults
    error = 0
    for section in config.sections():
        if config.get(section, 'method') == "local":
            config.set(section, 'fqdn', "localhost")
        if (
                not config.has_option(section, 'fqdn') and
                config.get(section, 'method') != "local"):
            sys.stderr.write(
                    "Config error: {} must have a fqdn set\n".format(section))
            error = 1
        if not config.has_option(section, 'incoming'):
            sys.stderr.write((
                    "Config error: {} must have an incoming directory set\n"
                    ).format(section))
            error = 1
    if error:
        sys.exit(1)

    return config


def print_config(config, debug):
    """ Print the configuration and exit.

        :param config: `ConfigParser` instance for this application.
        :param debug: If true, enable debugging output.
        :return: ``None``.
        """
    sys.stdout.write("\n")
    config.write(sys.stdout)
    sys.stdout.write("\n")


def print_default_upload_method(config):
    """ Print the default upload method defined in the configuration.

        :param config: `ConfigParser` instance for this application.
        :return: ``None``.
        """
    method_names_by_config_section = get_upload_method_names(
            config, host=None)
    default_method = method_names_by_config_section[config.default_section]
    sys.stdout.write(textwrap.dedent("""\

            Default Method: {}

            """.format(default_method)))


def print_host_list(config):
    """ Print a list of hosts defined in the configuration.

        :param config: `ConfigParser` instance for this application.
        :return: ``None``.
        """
    for section in config.sections():
        distributions = ""
        if config.get(section, 'distributions'):
            distributions = ", distributions: {}".format(
                    config.get(section, 'distributions'))
        sys.stdout.write((
                "{section} => {fqdn}"
                "  (Upload method: {method}{distributions})\n"
                ).format(
                    section=section,
                    fqdn=config.get(section, 'fqdn'),
                    method=config.get(section, 'method'),
                    distributions=distributions))
    sys.stdout.write("\n")


def get_upload_method_names(config, *, host):
    """ Get the configured upload method name for host and default.

        :param config: The `ConfigParser` instance for this application.
        :param host: The remote host for which to get the configuration.
        :return: A mapping `{section_name: upload_method_name}`, where the keys
            `section_name` are each of `config.default_section` and `host`.
            Each `upload_method_name` is the corresponding configured value in
            that section, or ``None`` if not present in `config`.
        """
    result = {
            section_name: config.get(section_name, 'method', fallback=None)
            for section_name in [config.default_section, host]
            }
    return result


# Copyright © 2016–2025 Ben Finney <bignose@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :
