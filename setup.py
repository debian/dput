# setup.py
# Part of ‘dput’, a Debian package upload toolkit.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

""" Distribution setup for ‘dput’ library. """

import email.utils
import os.path
import re
import pydoc

import debian.changelog
import debian.copyright
import debian.deb822
import packaging.version
from setuptools import setup


debian_version_regex = re.compile(
    r"""
        # Anchor at start of string.
        ^
        # Optional: Epoch (digit sequence), followed by ‘:’.
        ((?P<epoch>\d+):)?
        # The version string of the release.
        (?P<release_version>
            # Upstream version string.
            (?P<upstream_version>
                # Match only if this segment begins with a digit.
                (?=\d)
                # Characters including ‘-’, ‘~’, ‘+’, only if all of those
                # characters follow.
                ([\w.~+-]+(?=[-])(?![-]+)(?=[~])(?![~-]+)(?=[+])(?![~+-]+))
                # Alternately, match the next group.
                |
                # Characters including ‘-’, ‘~’, only if all of those
                # characters follow.
                ([\w.~-]+(?=[-])(?![-]+)(?=[~])(?![~-]+))
                # Alternately, match the next group.
                |
                # Characters including ‘-’, ‘+’, only if all of those
                # characters follow.
                ([\w.+-]+(?=[-])(?![-]+)(?=[+])(?![~+-]+))
                # Alternately, match the next group.
                |
                # Characters including ‘-’, only if that character follows.
                ([\w.-]+(?=[-])(?![-]+))
                # Alternately, match the next group.
                |
                # Characters including ‘~’, ‘+’, only if all of those
                # characters follow.
                ([\w.~+]+(?=[~])(?![~-]+)(?=[+])(?![~+-]+))
                # Alternately, match the next group.
                |
                # Characters including ‘~’, only if that character follows.
                ([\w.~]+(?=[~])(?![~-]+))
                # Alternately, match the next group.
                |
                # Characters including ‘+’, only if that character follows.
                ([\w.+]+(?=[+])(?![~+-]+))
                # Alternately, match the next group.
                |
                # Characters excluding all of ‘-’, ‘~’, ‘+’.
                ([\w.]+)
            )
            # Optional: ‘-’ followed by Debian release number (alphanumerics).
            (-(?P<debian_revision>
                # Characters including ‘~’, ‘+’, only if all of those
                # characters follow.
                ([\w.~+]+(?=[~])(?![~]+)(?=[+])(?![~+]+))
                # Alternately, match the next group.
                |
                # Characters including ‘~’, only if that character follows.
                ([\w.~]+(?=[~])(?![~]+))
                # Alternately, match the next group.
                |
                # Characters including ‘+’, only if that character follows.
                ([\w.+]+(?=[+])(?![+]+))
                # Alternately, match the next group.
                |
                # Characters excluding all of ‘-’, ‘~’, ‘+’.
                ([\w.]+)
            ))?
        )
        # Optional: ‘~’ followed by prerelease segment (indicator this version
        # is *prior* to `release_version`).
        (~(?P<prerelease_segment>
            # Sequence of ASCII alphabetic characters.
            (?P<prerelease_prefix>[A-Za-z]+)
            # Sequence of ASCII digits.
            (?P<prerelease_ordinal>\d+)
        ))?
        # Optional: Post-release annotation of the version after release,
        # identifying subsequent re-builds, etc.
        # ‘+’ followed by alphanumerics and other version characters.
        (\+(?P<annotation>[\w.+:-]+))?
        # Anchor at end of string.
        $
    """,
    (re.VERBOSE | re.ASCII))
""" Regular expression pattern for a Debian package version.

    The match groups allow extracting specifically those parts relevant for
    composing the corresponding Python package version.

    <URL:https://www.debian.org/doc/debian-policy/ch-controlfields.html#version>
    """

python_prerelease_regex = re.compile(
    r"""
        # Anchor at start of string.
        ^
        # Specific values allowed for pre-release segment prefix.
        (?P<prefix>a|alpha|b|beta|rc|c|pre|preview)
        # Sequence of digits.
        (?P<ordinal>\d+)
        # Anchor at end of string.
        $
    """,
    (re.VERBOSE | re.ASCII))
""" Regular expression pattern for a PyPA conformant pre-release segment.

    <URL:https://packaging.python.org/specifications/version-specifiers/>
    """


def packaging_version_from_debian_version(debian_version):
    """ Make valid Python PEP 440 version, from `debian_version`.

        :param debian_version: A `debian.changelog.Version` object representing
            the version of the Debian package.
        :return: A valid Python `packaging` version text representing the
            `debian_version` as a valid PEP 440 version text.
        """
    match = debian_version_regex.match(str(debian_version))
    if match is None:
        raise ValueError(
            "Debian package version {version!r}"
            " does not match expected pattern {pattern!r}".format(
                version=debian_version,
                pattern=debian_version_regex,
        ))
    prerelease_segment = match.group('prerelease_segment')
    if prerelease_segment is not None:
        prerelease_match = python_prerelease_regex.match(
            str(prerelease_segment))
        if prerelease_match is None:
            raise ValueError(
                "Prerelease segment {prerelease!r}"
                " does not match expected pattern {pattern!r}".format(
                    version=prerelease_segment,
                    pattern=python_prerelease_regex,
                ))
    packaging_prerelease = (
        "" if prerelease_segment is None
        else prerelease_segment if prerelease_match
        else "dev{}".format(match.group('prerelease_ordinal'))
    )
    packaging_version = "{release}{prerelease}".format(
        release=match.group('release_version'),
        prerelease=packaging_prerelease,
    )
    # Verify that `packaging_version` is a valid Python package version.
    try:
        __ = packaging.version.Version(packaging_version)
    except packaging.version.InvalidVersion as exc:
        raise ValueError(
            "Package release version {version!r}"
            " is not a valid Python package version".format(
                version=packaging_version)) from exc
    return packaging_version


setup_dir = os.path.dirname(__file__)

readme_file_path = os.path.join(setup_dir, "README")
with open(readme_file_path, encoding='UTF-8') as readme_file:
    (synopsis, long_description) = pydoc.splitdoc(readme_file.read())

changelog_file_path = os.path.join(setup_dir, "debian", "changelog")
with open(changelog_file_path, encoding='UTF-8') as changelog_file:
    changelog = debian.changelog.Changelog(changelog_file, max_blocks=1)
(author_name, author_email) = email.utils.parseaddr(changelog.author)
version = packaging_version_from_debian_version(changelog.version)

control_file_path = os.path.join(setup_dir, "debian", "control")
with open(control_file_path, encoding='UTF-8') as control_file:
    control_structure = debian.deb822.Deb822(control_file)
(maintainer_name, maintainer_email) = email.utils.parseaddr(
        control_structure['maintainer'])

copyright_file_path = os.path.join(setup_dir, "debian", "copyright")
with open(copyright_file_path, encoding='UTF-8') as copyright_file:
    copyright_structure = debian.copyright.Copyright(copyright_file)
general_files_paragraph = copyright_structure.find_files_paragraph("*")
license = general_files_paragraph.license


setup_kwargs = dict(
        # Core project metadata.
        version=version,
        description=synopsis,
        long_description=long_description,
        author=author_name,
        author_email=author_email,
        maintainer=maintainer_name,
        maintainer_email=maintainer_email,
        license=license.synopsis,
        url=control_structure['homepage'],
        )


if __name__ == '__main__':  # pragma: nocover
    setup(**setup_kwargs)


# Copyright © 2008–2025 Ben Finney <ben+python@benfinney.id.au>
#
# This is free software: you may copy, modify, and/or distribute this work
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 3 of that license or any later version.
# No warranty expressed or implied. See the file ‘LICENSE.GPL-3’ for details.


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :
