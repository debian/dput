# test/test_setup.py
# Part of ‘dput’, a Debian package upload toolkit.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

""" Unit tests for Python Setuptools configuration module ‘setup’. """

import debian.debian_support
import packaging.version
import testscenarios
import testtools

import setup

from .helper import make_expected_error_context


class packaging_version_from_debian_version_TestCase(
        testscenarios.WithScenarios,
        testtools.TestCase):
    """ Test cases for function `packaging_version_from_debian_version`. """

    function_to_test = staticmethod(
        setup.packaging_version_from_debian_version)

    epoch_scenarios = [
        ('no-epoch', {
            'test_version_prefix': "",
        }),
        ('with-epoch', {
            'test_version_prefix': "2:",
        }),
        ('epoch-invalid', {
            'test_version_prefix': "beans:",
            'expected_error': ValueError,
        }),
        ('prefix-invalid', {
            'test_version_prefix': "b0gUs",
            'expected_error': ValueError,
        }),
    ]

    release_version_scenarios = [
        ('simple-one', {
            'test_version_release': "3",
            'expected_packaging_version_main': "3",
        }),
        ('simple-two', {
            'test_version_release': "1.3",
            'expected_packaging_version_main': "1.3",
        }),
        ('simple-three', {
            'test_version_release': "1.3.5",
            'expected_packaging_version_main': "1.3.5",
        }),
        ('release-invalid', {
            'test_version_release': "b@gU$",
            'expected_error': ValueError,
        }),
    ]

    prerelease_scenarios = [
        ('no-prerelease', {
            'test_version_prerelease': "",
            'expected_packaging_version_prerelease': "",
        }),
        ('prerelease-alpha1', {
            'test_version_prerelease': "~alpha1",
            'expected_packaging_version_prerelease': "alpha1",
        }),
        ('prerelease-a1', {
            'test_version_prerelease': "~a1",
            'expected_packaging_version_prerelease': "a1",
        }),
        ('prerelease-rc1', {
            'test_version_prerelease': "~rc1",
            'expected_packaging_version_prerelease': "rc1",
        }),
        ('prerelease-lorem1', {
            'test_version_prerelease': "~lorem1",
            'expected_error': ValueError,
        }),
        ('prerelease-invalid', {
            'test_version_prerelease': "~eggs",
            'expected_error': ValueError,
        }),
        ('suffix-invalid', {
            'test_version_prerelease': "b0gUs",
            'expected_error': ValueError,
        }),
    ]

    annotation_scenarios = [
        ('no-annotation', {
            'test_version_annotation': "",
        }),
        ('annotation-real-version', {
            'test_version_annotation': "+really1.2",
        }),
        ('annotation-nmu', {
            'test_version_annotation': "+nmu2",
        }),
        ('annotation-dfsg', {
            'test_version_annotation': "+dfsg2",
        }),
        ('annotation-binary', {
            'test_version_annotation': "+b2",
        }),
        ('annotation-ci-chatter', {
            'test_version_annotation': "+salsaci+20250216+16",
        }),
        ('annotation-invalid', {
            'test_version_annotation': "+b@gU$",
            'expected_error': ValueError,
        }),
    ]

    scenarios = testscenarios.multiply_scenarios(
        epoch_scenarios,
        release_version_scenarios,
        prerelease_scenarios,
        annotation_scenarios,
    )

    for (scenario_name, scenario) in scenarios:
        test_version = (
            debian.debian_support.Version(
                "{prefix}{release}{prerelease}{annotation}".format(
                    prefix=scenario['test_version_prefix'],
                    release=scenario['test_version_release'],
                    prerelease=scenario['test_version_prerelease'],
                    annotation=scenario['test_version_annotation'],
                ))
            if 'expected_error' not in scenario
            else object()
        )
        scenario['test_version'] = test_version
        scenario['test_args'] = [scenario['test_version']]
        if 'expected_error' not in scenario:
            scenario['expected_result'] = (
                "{main}{suffix}".format(
                    main=scenario['expected_packaging_version_main'],
                    suffix=scenario['expected_packaging_version_prerelease'],
                ))

    def test_returns_expected_result_or_raises_exception(self):
        """ Should return expected result or raise expected exception. """
        with make_expected_error_context(self):
            result = self.function_to_test(*self.test_args)
        if not hasattr(self, 'expected_error'):
            self.assertEqual(self.expected_result, result)

    def test_result_is_valid_python_packaging_version(self):
        """ Should return a valid Python packaging version. """
        if hasattr(self, 'expected_error'):
            self.skipTest("no expected result in this scenario")
        result = self.function_to_test(*self.test_args)
        __ = packaging.version.Version(result)


# Copyright © 2015–2025 Ben Finney <bignose@debian.org>
#
# This is free software: you may copy, modify, and/or distribute this work
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 3 of that license or any later version.
# No warranty expressed or implied. See the file ‘LICENSE.GPL-3’ for details.


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :
